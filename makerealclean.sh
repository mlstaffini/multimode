#!/bin/bash

rm -f *.cc
rm -f *.o
#rm *.h5
#rm *.xsil
rm -f *~
rm -f \#*
rm -f *realspace *realspace-atoms-only realspace-1D


echo "Done."
