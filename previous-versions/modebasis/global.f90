MODULE global
  IMPLICIT NONE

  ! Global parameters:
  INTEGER, PARAMETER :: DP=KIND(1.0D0)
  COMPLEX (KIND=DP), PARAMETER :: ii=(0.0D0,1.0D0)
  REAL (KIND=DP), PARAMETER ::  Pi=3.14159265358979d0

  ! Parameters to read in:
  INTEGER :: n_max, nu_max, m_max, m_set, statesize, mathOutput
  REAL(KIND=DP) :: z_0, z_R, z_M, lambda_0, omega_0, omega_T, m_A, E_0, &
       & sigma_pump, omega_pump, Delta_0, w0, w_z0, l_ho, tinit, tend, &
       & tstep, kappa_min, hbar, convergence_threshold, eta, f_0

  ! Ports:
  INTEGER, PARAMETER :: AlphaPort=48, PsiPort=49, ChiZeroPort=50, &
       & ChiMNZPort=51

  !Filenames:
  CHARACTER (LEN=128) :: AlphaFile, PsiFile

  ! State variables to evolve:
  COMPLEX(KIND=DP), ALLOCATABLE :: Psi(:,:), Alpha(:,:)
  REAL (KIND=DP) , ALLOCATABLE :: CurrentState(:)
  REAL(KIND=DP), ALLOCATABLE ::  Chi_zero_m(:,:,:,:), Chi_match_wz0(:,:,:,:), &
       & Chi_match_lho(:,:,:,:), Chi_opp_wz0(:,:,:,:), Chi_opp_lho(:,:,:,:), &
       & Chi_first_third_match(:,:,:,:), Chi_first_third_opp(:,:,:,:)
  LOGICAL :: linstab, steadystate, test_discrepancy_1, test_discrepancy_2

CONTAINS

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: encode_state
  ! VARIABLES:
  !           psi   - the phi matrix (IN)
  !           alpha   - the alpha coefficients matrix (IN)
  !           state - the resulting state vector (OUT)
  ! SYNOPSIS:
  !   The routine flattens the matrices into a state vector for the
  !   NAG routines to handle.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  SUBROUTINE encode_state(psi, alpha, state)
    COMPLEX (KIND=DP), INTENT(IN) :: psi(0:nu_max,0:m_max), alpha(0:n_max,0:m_max)
    REAL (KIND=DP), INTENT(OUT) :: state(*)

    INTEGER :: size_nu, size_n

    size_nu=(m_max+1)*(nu_max+1); size_n=(m_max+1)*(n_max+1)
    
    state(1:size_nu)=RESHAPE(REAL(psi), (/ size_nu /))
    state(size_nu+1:2*size_nu)=RESHAPE(AIMAG(psi), (/ size_nu /))
    state(2*size_nu+1:2*size_nu+size_n)=RESHAPE(REAL(alpha), (/ size_n /))
    state(2*size_nu+size_n+1:2*size_nu+2*size_n)=RESHAPE(AIMAG(alpha), (/ size_n /))
            
  END SUBROUTINE encode_state
 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: decode_state
  ! VARIABLES:
  !           psi   - the phi matrix (OUT)
  !           alpha   - the alpha coefficients matrix (OUT)
  !           state - the encoded state vector (IN)
  ! SYNOPSIS:
  !   Extracts phi and alpha matrix from state vector.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  SUBROUTINE decode_state(state, psi, alpha)
    COMPLEX (KIND=DP), INTENT(OUT) :: psi(0:nu_max,0:m_max), alpha(0:n_max,0:m_max)
    REAL (KIND=DP), INTENT(IN) :: state(*)

    INTEGER :: size_nu, size_n
    
    size_nu=(m_max+1)*(nu_max+1); size_n=(m_max+1)*(n_max+1)
    
    psi=RESHAPE(state(1:size_nu), (/ nu_max+1, m_max+1 /)) + &
         & ii*RESHAPE(state(size_nu+1:2*size_nu), (/ nu_max+1, m_max+1 /))

    alpha=RESHAPE(state(2*size_nu+1:2*size_nu+size_n), (/ n_max+1, m_max+1 /)) +&
         & ii * RESHAPE(state(2*size_nu+size_n+1:2*size_nu+2*size_n), (/ n_max+1, m_max+1 /))
      
  END SUBROUTINE decode_state
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: fact
  ! VARIABLES:
  !           n
  ! SYNOPSIS:
  !   Returns n!.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION fact(n)
    INTEGER, INTENT(IN) :: n
    REAL (KIND=DP) :: fact

    INTEGER :: i

    if (n==0) then
       fact=1
    else
       fact=PRODUCT((/(DBLE(i), i=1,n)/))
    end if
    
  END FUNCTION fact

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   delta
  ! VARIABLES:
  !           x,y - arguments of delta fn
  ! SYNOPSIS:
  !   Delta function.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION deltafn(x,y)
    INTEGER, INTENT(IN) :: x,y
    INTEGER :: deltafn

    if (x==y) then
       deltafn=1
    else
       deltafn=0
    end if
    
  END FUNCTION deltafn

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: kappa
  ! VARIABLES:
  !           m
  !           n
  ! SYNOPSIS:
  ! Loss rate as a function of mode order.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION kappa(n,m)
    REAL (KIND=DP) :: kappa, A,B
    INTEGER, INTENT(IN) :: m,n

    REAL (KIND=DP) :: inv_kappa_fn

    ! Guessing something for now that is one at the centre and tails off 
    ! to zero to two (0.0013) decimal places at m=50
    A=1/3200
    B=3
    inv_kappa_fn=exp(-A*m**B-A*n**B)

    kappa=kappa_min*(1/inv_kappa_fn)

  END FUNCTION kappa


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: LogGamma
  ! VARIABLES:
  !           x
  !           ifail
  ! SYNOPSIS:
  !   Returns the Log of a gamma function.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION LogGamma(x)
    USE nag_library, ONLY: S14ABF
    REAL (KIND=DP) :: LogGamma
    INTEGER, INTENT(IN) :: x

    INTEGER :: ifail

    ifail=0

    IF (x==0) THEN
       LogGamma=1
    ELSE
       LogGamma=S14ABF(DBLE(x), ifail)
    END IF
    
  END FUNCTION LOGGAMMA

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
  ! NAME: write_output
  ! VARIABLES:
  !           t    - the physical time t (IN)
  ! SYNOPSIS:
  !   This subroutine writes the relevant information to file.
  !   NOTE - This also updates the current state by calling decode.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  SUBROUTINE write_output(t)
    REAL (KIND=DP), INTENT(IN) :: t

    CHARACTER (len=45) :: format_string1, format_string2

    CALL decode_state(CurrentState, Psi, Alpha)

    !~~ Writing Psi: number of colums is (nu_max+1) times two (complex).
    ! Writing the string containing the format of the columns:
    format_string1=""
    WRITE(format_string1, '(A15, I2, A24 )') '(X, F7.3, 4X' , (nu_max+1), '(D16.6, X, D16.6, 3X))'
    format_string1=TRIM(format_string1)

    !~~ Writing to file:
    ! Steady state:
    IF (steadystate) THEN
       WRITE(49, format_string1) t, Psi
       
       ! LINEAR STABILITY 
    ELSE
       PRINT*, "No lin stab writing routine yet"
    END IF

    !~~~ Writing Alpha: number of colums is n_max+1 times two (complex).    
    ! Writing the string containing the format of the columns:
    format_string2=""
    WRITE(format_string2, '(A15, I2, A24)') "(X, F7.3, 4X", (n_max+1), TRIM("(D16.6, X, D16.6, 3X))")
    format_string2=TRIM(format_string2)

       !~~ Writing to file:
       ! Steady state:
       IF (steadystate) THEN
          WRITE(AlphaPort, format_string2) t, Alpha(:, 0)

          ! LINEAR STABILITY   
       ELSE
          PRINT*, "No lin stab writing routine yet"
       END IF

       ! IF ALSO WRITING TO TERMINAL:
       WRITE(*, '(F7.3, 3X, D16.6, 3X, D16.6)') t, SUM(ABS(Psi(:,0))**2), SUM(ABS(Alpha(:,0))**2)

       

  END SUBROUTINE write_output


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   dump_params
  ! VARIABLES:
  !           port
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE dump_params(port)
    INTEGER, INTENT(IN) :: port

    ! Write parameters to pre-opened file:
    WRITE(port, '("# Lenghts (micrometers): ", a3, f6.1,  a5, F6.1, a11, F4.3, &
         & a10, F6.2)') "z0=", z_0, ", zR=", z_R, ", Lambda_0=", lambda_0, &
         & ", sigma_P=", sigma_pump

    WRITE(port, '("# Lengthscales (micrometers): "a6,f10.5,a8,f10.5)') " l_HO=",l_ho, &
         & ", w(z0)=", w_z0

    WRITE(port, '("# Frequencies (MHz):", a10, d10.5, a6, d10.5, a10, d10.5, a10, d10.5)') &
         & " Delta_0=", Delta_0, ", E_0=", E_0, ", kappa_0=", kappa_min, &
         & ", omega_T=", omega_T

    WRITE(port, '("# Run parameters:", 3(a8,I2),a12,f8.4)'),"n_max=", n_max,&
         & ", nu_max=", nu_max, ", m_max=", m_max, ", runtime=", tend


    WRITE(port, *) "   "
    
  END SUBROUTINE dump_params

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: make_file
  ! VARIABLES:
  !           
  ! SYNOPSIS:
  !   Sets up output files and writes headers depending on sweep type.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE make_file(type,port,namestring)
    CHARACTER(LEN=*), INTENT(IN) :: type
    INTEGER, INTENT(IN) :: port
    CHARACTER(LEN=*), INTENT(OUT) :: namestring

    LOGICAL :: ex

    SELECT CASE(type)

  !~~~~~~~~~~ Create file for Alpha vector:
    CASE ('alpha')

       IF (steadystate) THEN
          ! Write the filename variable:
          WRITE(namestring,'(a25, I2, a7, f6.2, a4)')"Alpha-steady-state-n_max=" &
               & ,n_max,"-at_z0=",z_0,".dat"
          namestring=TRIM(namestring)
       ELSE          
          ! Write the filename variable:
          WRITE(namestring,"(a13, I2, a7, I2, a7, f6.2, a4)") &
               & "Alpha-n_max=",n_max,"-m_max=",m_max,"-at_z0=",z_0,".dat"
          namestring=TRIM(namestring)
       END IF

       ! Create the file:
       INQUIRE(file=namestring, exist=ex)
       IF (ex) THEN           
          OPEN(port, file=namestring, status='replace')
       ELSE
          OPEN(port, file=namestring, status='new')
       END IF
       
       ! Dump parameters and write the headers:
       CALL dump_params(port)
       WRITE(port,'("#",2X,A8,4X,A30)') "Time(ms)", "Alpha entries (complex pairs)"

    !~~~~~~~~~~ Create file for Psi vector:
    CASE ('psi')

       IF (steadystate) THEN
          ! Write the filename variable:
          WRITE(namestring,"(a23, I2, a7, f6.2, a4)") &
               & "Psi-steady-state-n_max=",n_max,"-at_z0=",z_0,".dat"
          namestring=TRIM(namestring)
       ELSE          
          ! Write the filename variable:
          WRITE(namestring,"(a11, I2, a7, I2, a7, f6.2, a4)") &
               & "Psi-n_max=",n_max,"-m_max=",m_max,"-at_z0=",z_0,".dat"
          namestring=TRIM(namestring)
       END IF

       ! Create the file:
       INQUIRE(file=namestring, exist=ex)
       IF (ex) THEN           
          OPEN(port, file=namestring, status='replace')
       ELSE
          OPEN(port, file=namestring, status='new')
       END IF
       
       ! Dump parameters and write the headers:
       CALL dump_params(port)
       WRITE(port,'("#",2X,A8,4X,A28)') "Time(ms)", "Psi entries (complex pairs)"

    END SELECT

    CLOSE(port)
    
  END SUBROUTINE make_file
      

END MODULE global
