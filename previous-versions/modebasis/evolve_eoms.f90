MODULE run_diff_eqns
  USE global, ONLY: statesize, Psi, Alpha, CurrentState, tinit, tend, tstep, &
       & convergence_threshold, n_max, nu_max, m_max, AlphaPort, PsiPort, &
       & decode_state, encode_state, write_output
  USE setup_eoms, ONLY: diffeqn
  IMPLICIT NONE

  INTEGER, PARAMETER :: DP=KIND(1.0D0)
  
  INTEGER, PARAMETER :: checklog = 50
  REAL (KIND=DP) :: Store(checklog)

CONTAINS

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: find_steady
  ! VARIABLES:
  !           state  - encoded state (INOUT)
  !           xinit  - initial state to pass to NAG (INOUT)
  !           xend   - final state from NAG (INOUT)
  !           step   - timestep
  !           buffer - time before code starts checking for 
  !                                               convergence (IN)
  ! SYNOPSIS:
  !   This evolves the state until a steady state is reached 
  !   (|ds_dt|< tolerance), or returns an error if no steady
  !   state is reached by xend.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   

  SUBROUTINE find_steady(state, xinit, xend, step)
    REAL (KIND=DP), INTENT(INOUT) :: xinit, xend, state(0:statesize)
    REAL (KIND=DP), INTENT (IN) :: step
    
    REAL (KIND=DP) :: dstate(0:statesize)
    
    ! Temp t variables: initial, end and current time in loop
    REAL (KIND=DP) :: tempinit, tempend, t_temp
    INTEGER :: i, buffer

    buffer=2
    
    tempinit=xinit; tempend=xend

    PRINT*, "Evolving EoMs looking for steady state..."

    CALL write_output(tempinit)
    
    ! Initial state may change slowly - run for some time without 
    ! checking for convergence:
    DO i=1, buffer
        t_temp = tempinit + step
        CALL evolve_state(state, tempinit, t_temp)
        CALL storage(checklog)
        tempinit = t_temp
        CALL write_output(tempinit)
     END DO

    ! Now check for convergence, exiting if reached steady state or max time:    

     time_evo_loop: DO WHILE(.NOT. converged())
       t_temp = tempinit + step
       CALL evolve_state(state, tempinit, t_temp)
       CALL storage(checklog)
       tempinit = t_temp
       CALL write_output(tempinit)
       
       ! If it hits maximum time limit:
       IF (tempinit .GE. tempend) THEN
          WRITE(AlphaPort,'("# Warning: did not reach a steady state.")')
          WRITE(PsiPort,'("# Warning: did not reach a steady state.")')
          PRINT*, "Warning: did not reach steady state in allocated time."
          EXIT time_evo_loop
       END IF

    END DO time_evo_loop
    
  END SUBROUTINE find_steady

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: evolve_state
  ! VARIABLES:
  !           state  - encoded state (INOUT)
  !           xinit  - initial state to pass to NAG (INOUT)
  !           xend   - final state from NAG (INOUT)
  ! SYNOPSIS:
  !   This subroutine evolves the state from xinit to xend according 
  !   to eqns of motion.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  SUBROUTINE evolve_state(state, xinit, xend)
    USE nag_library, ONLY: d02bjf, d02bjw, d02bjx!, D02EJF, D02EJY, D02EJX, D02EJW
    
    REAL (KIND=DP), INTENT(INOUT) :: xinit, xend, state(statesize)
    
    REAL (KIND=DP) :: tol, W(20*statesize), WW((12+statesize)*statesize+100)
    INTEGER :: i, NN!, iW
    INTEGER :: ifail = 0

    ! NAG parameters:
    NN = statesize; tol = 0.001 !; iW=(12+statesize)*statesize+100

    !Now evolve the equation to the end point.
    !Output and stopping both suppressed.
    CALL d02bjf(xinit, xend, NN, state, diffeqn, tol, 'D', &
              & d02bjx, d02bjw, W, ifail)

    !CALL D02EJF (xinit, xend, NN, state, diffeqn, D02EJY, tol, 'D', &
    !     & D02EJX, D02EJW, WW, IW,  ifail)
    
  END SUBROUTINE evolve_state

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: storage
  ! VARIABLES:
  !           N - the number of elements stored (IN)
  ! SYNOPSIS:
  !   This subroutine stores the last N values of a chosen element 
  !   of the state. 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  SUBROUTINE storage(N)
    COMPLEX (KIND=DP) :: PsiTemp(0:n_max,0:m_max), AlphaTemp(0:nu_max,0:m_max)
    INTEGER :: i, N
    
    CALL decode_state(CurrentState, PsiTemp, AlphaTemp)
   
    DO i = 1, (N - 1)
       Store(i) = Store(i+1)
    END DO
   
    Store(N)=SUM(ABS(AlphaTemp))
    
  END SUBROUTINE storage
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: converged
  ! VARIABLES:
  ! SYNOPSIS:
  !   This function is true if the state has converged to steady state 
  !   (the fluctuations in values is below tolerance).
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION converged()
    LOGICAL :: converged
    INTEGER :: i
    REAL (KIND=DP) :: maximum, minimum
    
    converged = .TRUE.
    maximum = maxval(Store)
    minimum = minval(Store)
    
    IF ( ( (maximum - minimum) .GT. convergence_threshold) ) THEN
       converged = .FALSE.
    END IF
    
    
  END FUNCTION converged

END MODULE run_diff_eqns
