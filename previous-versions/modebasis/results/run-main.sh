#!/bin/bash

#Should be in shell now
#export GFORTRAN_UNBUFFERED_ALL="Y"

# Variable parameters:

n_max=1
nu_max=1
m_max=0

z_0=0

#lho_wz0_ratio=1

# Quantity eta=sqrt(2)*lho/wz0:
eta=1
#eta=$(echo "sqrt ( 2 )" | bc -l)

f_0=1

mathOutput=65

. common-functions.sh

main()
{

    chitablename="Chi_lookup_m=0_n=${n_max}_nu=${nu_max}.dat"
    steadystatedir="SteadyState_n=${n_max}_nu=${nu_max}_z0=${z_0}_f0=${f_0}"
    linstabdir="LinStab_n=${n_max}_nu=${nu_max}_m=${m_max}_z0=${z_0}_f0=${f_0}"
     
    if [ $m_max -eq 0 ]; then
	NEWDIR=$steadystatedir
    else
	NEWDIR=$linstabdir
    fi
    
    setup_other_params
    setup_new_directory

    # If the steady state exists copies time evo files to lin stab directory:
    if [! $m_max -eq 0]; then
	if [-d ../$steadystatedir]; then
	    cp Alpha*.dat ../$steadystatedir .
	    cp Psi*.dat ../$steadystatedir .
	fi
    fi

    # If the chi table for m=0 exists copies it to within folder:
    if [ -a ~/Code/multimode-mls/results/$chitablename ]; then
	cp ~/Code/multimode-mls/results/$chitablename .
	mv $chitablename "Chi_lookup_m=0.dat"
    fi    
    
    create_and_run_script

    if [ ! -a ~/Code/multimode-mls/results/$chitablename ]; then
	cp "Chi_lookup_m=0.dat" $chitablename
	mv $chitablename ..
    fi
    
    cd ..
    
}

main
