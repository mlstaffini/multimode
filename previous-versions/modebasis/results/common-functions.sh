setup_new_directory() {

    mkdir -p $NEWDIR
    cd $NEWDIR/
    ln -sf ../../main ./
    
    rm -f *.dat

}


create_and_run_script() {
    cat <<EOF > $CONTROL
$z_R, $lambda_0, $sigma_pump
$Delta_0, $E_0, $kappa_min
${f_0}
$tinit, $tend, $tstep
$convergence_threshold
$n_max, $nu_max, $m_max
$z_0
$eta
EOF

./main < $CONTROL

}


cleanup_data() {
    tar -czf "$NEWDIR-Evol.tar.gz" UP* DN*
    rm UP* DN*
}


setup_other_params() {

CONTROL=PARAMS

z_R=5000
lambda_0=0.780 
sigma_pump=100 

Delta_0=10
E_0=40
kappa_min=0.8

tinit=0
tend=500
tstep=1

convergence_threshold=0.002

}

