PROGRAM multimode
  USE global, ONLY: Pi, z_0, z_R, lambda_0, n_max, nu_max, m_max, &
                  & omega_T, m_A, sigma_pump, Delta_0, &
                  & w0, w_z0, l_ho, E_0, Alpha, Psi, CurrentState, &
                  & Chi_zero_m, Chi_match_wz0, Chi_match_lho, m_set, &
                  & Chi_opp_wz0, Chi_opp_lho, Chi_first_third_match, &
                  & Chi_first_third_opp, tinit, tend, tstep, linstab, &
                  & steadystate, z_M, statesize, kappa_min, hbar, &
                  & make_file, AlphaFile, AlphaPort, PsiFile, PsiPort, &
                  & deltafn, fact, convergence_threshold, eta, &
                  & encode_state, decode_state, f_0, test_discrepancy_1, &
                  & test_discrepancy_2, kappa
  USE laguerre, ONLY: int_chi_lookup, do_integral
  USE setup_eoms, ONLY: set_initial, diffeqn, Pump
  USE run_diff_eqns, ONLY: find_steady
  IMPLICIT NONE

  INTEGER, PARAMETER :: DP=KIND(1.0D0)

  REAL (KIND=DP) :: Chi_all_zeros_1, Chi_all_zeros_2, Chi_all_zeros_3, &
       & time_start, time_finish, Diff, r_array(280), lho_wz0_ratio
  REAL (KIND=DP), ALLOCATABLE :: OutState(:), Dummy(:)
  INTEGER :: localn1,localn2,localnu1,localnu2,localm1,localm2,localmu1,localmu2, ERR, &
       & a,b,c,d, x, y
  LOGICAL :: chi_list_thing, testing_single_vals, run_program, zero_test, encode_test, &
       & diff_test, aliveAlpha, alivePsi
  
  PRINT*, "   "
  PRINT*, "##################################################"
  PRINT*, "Starting"

  
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  !                    READING IN PARAMETERS FROM SHELL                      !
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  ! Reading in parameters from shell code:
  
  ! Set lengths (micron)
  READ(*,*) z_R, lambda_0, sigma_pump
  
  ! Set frequencies (MHz)
  READ(*,*) Delta_0, E_0, kappa_min
  
  ! Set quantities:
  READ(*,*) f_0 ! MHz per unit length?
  
  ! Diff eqn quantities
  READ(*,*) tinit, tend, tstep
  READ(*,*) convergence_threshold

  ! Varying parameters:
  READ(*,*) n_max, nu_max, m_max
  READ(*,*) z_0
  READ(*,*) eta

  ! Printing some stuff to terminal for book-keeping:
  PRINT*, "Atom position z_0=",z_0, ", pump intensity f_0=",f_0
  PRINT*, "n_max=", n_max, ", nu_max=", nu_max  

  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  !                      SETTING UP OTHER VARIABLES                          !
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

  ! m will actually be spanned for lin stab but need fixed for steady state:	
  m_set=0 

  ! Constants:
  m_A=1.42D-25 ! kg
  hbar=1.0546D-34 ! m^2 kg / s

  ! Actual mirror location to account for non-confocality:
  z_M=z_R
  statesize=2*(n_max+1)*(m_max+1)+2*(m_max+1)*(nu_max+1)
  

  ! Setting logical variables determining operation:
  IF (m_set .EQ. 0) THEN
     linstab=.FALSE.
     steadystate=.TRUE.
  ELSE
     PRINT*, "Doing linear stability calculations up to m=", m_max
     linstab=.TRUE.
     steadystate=.FALSE.
  END IF
  
  ! Generating secondary parameters:
  lho_wz0_ratio=eta/SQRT(2.0D0)
  
  w0=sqrt(lambda_0*z_R/Pi) ! beam waist at cavity centre, micron
  
  w_z0=w0*sqrt(1+z_0/z_R) ! beam waist at atom position, micron
  
  l_ho=lho_wz0_ratio * w_z0 ! extracting lho from the ratio, micron

  omega_T=1d6*hbar/(l_ho**2 * m_A) ! from l_ho=sqrt(hbar/m_a*omega_T) (MHz)

  ! Print to terminal:
  PRINT*, "l_HO=", l_ho, ", w(z_0)=", w_z0, " (ratio eta is ", eta, ")"
  PRINT*, "omega_T=", omega_T

  ! Allocating the psi, alpha and state arrays:
  ALLOCATE(Psi(0:nu_max,0:m_max),Alpha(0:n_max,0:m_max), &
       & CurrentState(statesize), Dummy(statesize))

  ! Starting timer:
  CALL CPU_TIME(time_start)

  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  !                           TESTING STUFF                                  !
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 
  !~~~~~~~~~~~~~~~~~~~TESTING CHI TABLE

  chi_list_thing=.FALSE.
  
  IF (chi_list_thing) THEN
     PRINT*, "Printing chi list to file for checking..."

     OPEN(60, file="Chi_checking.dat", status='replace', action='write')
     WRITE(60, '("# ", 4(A3,X),2(3X,A7,5X))') "n1", "n2", "nu1", "nu2", &
          & "Int res", "Wolf Int"

     CALL generate_chi_zero_table()
     
     DO localn1=0, n_max
        DO localn2=0, n_max
           DO localnu1=0, nu_max
              DO localnu2=0, nu_max

                 PRINT*, "Calling do int"
                 ! Using integral routines
                 CALL do_integral(localn1,localn2,localnu1,localnu2,0,0,0,0, Chi_all_zeros_1)

                 PRINT*, "Writing out"
                 WRITE(60,'(x, 4(x,I2,X), 4(D15.6,X))') localn1, localn2, localnu1,&
                      & localnu2, Chi_all_zeros_1, &
                      & (Chi_zero_m(localn1, localn2, localnu1, localnu2)/(l_ho**2))

              END DO
           END DO
        END DO
     END DO
  END IF

  !~~~~~~~~~~~~~~~~~ TESTING SINGLE CHI VALUES (INT VS SUM1 VS SUM2)

  testing_single_vals=.FALSE.
  !test_discrepancy_1=.TRUE.
  test_discrepancy_1=.FALSE.
  !test_discrepancy_2=.TRUE.
  test_discrepancy_2=.FALSE.

  !OPEN(70, file="NewSum_checking.dat", status='replace', action='write')
  !OPEN(71, file="OldSum_checking.dat", status='replace', action='write')
  !WRITE(70, '("# ", 4(A2,X), A25)') "k1", "k2", "k3", "k4", &
  !     & "Sum term (no prefactors)"
  !WRITE(71, '("# ", 4(A2,X),5x, A9, X, A18, 4X, A9, 4X, A9)') "k1", "k2", "k3", "k4", &
  !     & "Old term", "Old term/lho^2", "New term", "Diff"

  ! multiplier every term in either dimless (1) or dimful (2) sum and then divides
  ! total integral term
  !multiplier=1d-5
  !PRINT*, "multiplier term:",  multiplier
  
  IF (testing_single_vals) THEN

     zero_test=.TRUE.

     if (zero_test) then
        localn1=0
        localn2=0
        localnu1=0
        localnu2=0

        PRINT*, "Checking Chi^4 for", localn1, localn2, localnu1, localnu2

        ! Using integral routines
        CALL do_integral(localn1,localn2,localnu1,localnu2,0,0,0,0, Chi_all_zeros_1)
        PRINT*, "Chi^4 calculated with integral:", Chi_all_zeros_1

     end if

     localn1=15
     localn2=20
     localnu1=20
     localnu2=14

     PRINT*, "Checking Chi^4 for", localn1, localn2, localnu1, localnu2

     ! Using integral routines
     CALL do_integral(localn1,localn2,localnu1,localnu2,0,0,0,0, Chi_all_zeros_1)
     PRINT*, "Chi^4 calculated with integral:", Chi_all_zeros_1

  END IF

  !~~~~~~~~~~~~~~~~~~~~~

  encode_test=.FALSE.

  IF (encode_test) THEN
     ! Testing reshape:

     Psi=0.0d0
     Alpha=0.0d0

     Psi(0,0)=(1.0d0,1.0d0)
     Psi(0,1)=(3.0d0,3.0d0)
     Psi(1,0)=(2.0d0,2.0d0)
     Psi(1,1)=(4.0d0,4.0d0)

     PRINT*, Psi
     PRINT*, "Psi(1,0) is", Psi(1,0)
     PRINT*, "Encoding..."
     CALL encode_state(Psi,Alpha,CurrentState)

     PRINT*, "Decoding..."
     CALL decode_state(CurrentState,Psi,Alpha)   
     PRINT*, Psi
     PRINT*, "Psi(1,0) is", Psi(1,0)
  END IF
  
  !~~~~~~~~~~~~~~
  ! Testing time evo

  diff_test=.FALSE.

  IF (diff_test) THEN
     CALL set_initial
     Dummy=0.0d0
     !PRINT*, CurrentState, Dummy
     
  ! Calculate or read in the chi integral tables:
     CALL int_chi_lookup()

     PRINT*, "E_0 is", E_0
     PRINT*, "Kappa(0) is", kappa(0,0)
     PRINT*, "Pump is"
     PRINT*, Pump()
     PRINT*, " "
     
     DO x=0,nu_max
        DO y=0, nu_max
           PRINT*, "Chi integral term for 0,0,", x, ",",y, "is ", Chi_zero_m(0,0,x,y)
        END DO
     END DO
     
     CALL diffeqn(5.0d0, CurrentState, Dummy)
     PRINT*, "ds_dt is", Dummy
  END IF
  
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  !                   RUNNING STEADY STATE TIME EVO                          !
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Calling all the bits:

  run_program=.TRUE.

  IF (run_program) THEN
     IF (steadystate) THEN
        PRINT*, "Doing steady state calculations..."

        ! Initialising states:
        CALL set_initial

        ! Making files for writing output:
        CALL make_file('alpha', AlphaPort, AlphaFile)
        CALL make_file('psi', PsiPort, PsiFile)
        PRINT*, "Output files:", AlphaFile, " and ", PsiFile
        
        OPEN(AlphaPort, file=AlphaFile, status='old', access='append')
        OPEN(PsiPort, file=PsiFile, status='old', access='append')

        ! Terminal message:
        WRITE(*, "('Running time evolution for m=',I2,A8,I2,A9,I2)"), &
             & m_set,", n_max=",n_max,", nu_max=", nu_max

        ! Calculate or read in the chi integral tables:
        CALL int_chi_lookup()

        ! Call the equilibrium finding differential routine:
        CALL find_steady(CurrentState,tinit,tend,tstep)
        PRINT*, "Done."

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   
     ! Linear stability part:
        
     ELSE IF (linstab) THEN
        PRINT*, "Doing linear stability calculations..."

        !Checking if there are files from the steady state:
        INQUIRE(file=AlphaFile, exist=aliveAlpha)
        INQUIRE(file=PsiFile, exist=alivePsi)
        
     END IF
  END IF
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  CALL CPU_TIME(time_finish)

  PRINT*, "##################################################"
  PRINT*, "   "

  WRITE(*,"('Total runtime: ',D10.2)") time_finish-time_start
  
CONTAINS

END PROGRAM multimode
