MODULE laguerre
  USE global, ONLY: m_max, n_max, fact, Chi_zero_m, Pi, deltafn, &
       & m_set, Chi_match_wz0, Chi_match_lho, Chi_opp_wz0, &
       & Chi_opp_lho, Chi_first_third_match, Chi_first_third_opp, nu_max, &
       & ChiMNZPort, ChiZeroPort, linstab, w_z0, l_ho, test_discrepancy_1, &
       & test_discrepancy_2
  USE nag_library, ONLY: d01akf, d01baf, d01bax
  IMPLICIT NONE
  
  INTEGER, PRIVATE, PARAMETER :: DP=KIND(1.0D0)
  INTEGER :: module_n1, module_n2, module_n3, module_n4,&
       & module_m1, module_m2, module_m3, module_m4, ERR
  REAL (KIND=DP) :: multiplier, Diff
  REAL (KIND=DP), ALLOCATABLE :: Bob(:,:,:,:)
  
CONTAINS

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: int_chi_lookup
  ! VARIABLES:
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE int_chi_lookup()
    INTEGER :: n1, n2, nu1, nu2
    REAL (KIND=DP) :: Chi_mnz(0:6*n_max,0:6*n_max,0:6*nu_max,0:6*nu_max)
    CHARACTER (LEN=32) :: namestring_chi
    LOGICAL :: alive, alive2, saving_private_chi

    ALLOCATE(Chi_zero_m(0:n_max,0:n_max,0:nu_max,0:nu_max))

    ! Use the option to write chi to file/read chi from file if existing:
    saving_private_chi=.TRUE.

    ! This sets logical variable 'alive' false/true if file exists or not 
    INQUIRE(file="Chi_lookup_m=0.dat", exist=alive)

    ! If there is already a calculated lookup table for m=0:
    IF (alive .AND. saving_private_chi) THEN

       PRINT*, "Existing chi lookup table for m=0 found, skipping integral computation."
       OPEN(ChiZeroPort, file="Chi_lookup_m=0.dat",  status='old', form= &
            & 'unformatted', access='sequential', action='read')
       READ(ChiZeroPort) Chi_zero_m

       ! If there is no lookup table, calculate one:
    ELSE

       PRINT*, "# Generating chi lookup tables for m = 0 ..."
       
       ! Write to file:
       OPEN(ChiZeroPort, file="Chi_lookup_m=0.dat", status='replace', form='unformatted', &
            & access='sequential', action='write')
       ! TEMP WRITING FOR DEBUG
       OPEN(77, file="Chi_lookup_m=0_TEMP.dat", status='replace', action='write')

       PRINT*, "[ % Complete                               ]"
      !PRINT*, "[#########|#########|#########|#########|##]
       
       WRITE(*, '(A2)', advance='no'), " ["

       ! The following allocates the lookup table for the zero m integral:
       DO n1=0,n_max    
          DO n2=0, n_max

             IF (n2==10) THEN
                WRITE(*,'(A1)',advance='no'), "#"
             END IF
             
             DO nu1=0, nu_max
                DO nu2=0, nu_max

                   ! Using integral routines
                   CALL do_integral(n1,n2,nu1,nu2,0,0,0,0, Chi_zero_m(n1,n2,nu1,nu2))
                   WRITE(77,'(4(I2,2X),D16.6)') n1,n2,nu1,nu2,Chi_zero_m(n1,n2,nu1,nu2)
                   
                END DO
             END DO
          END DO
          WRITE(*,'(A1)',advance='no'), "#"    
       END DO

       WRITE(*, '(A1)'), "]"  

       WRITE(ChiZeroPort) Chi_zero_m
       CLOSE(ChiZeroPort)
       CLOSE(77)
    END IF

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

    ! If there are angular momentum parts, additional tables are required: 
    IF (linstab) THEN

       ! Write the filename variable:
       WRITE(namestring_chi,"(a13, I2, a4)") &
            & "Chi_lookup_m=", m_set, ".dat"
       namestring_chi=TRIM(namestring_chi)

       INQUIRE(file=namestring_chi,exist=alive2)

       ! If lookup table for the given value of m exists just open the file:
       IF (alive2 .AND. saving_private_chi) THEN

          PRINT*, "Existing chi lookup table for m=",m_set , &
               & " found, skipping integrals computation."
          OPEN(ChiMNZPort,file=namestring_chi, status='old', form= &
               & 'unformatted', access='sequential', action='read')
          READ(ChiMNZPort) Chi_mnz
         
          Chi_match_wz0= Chi_mnz(0:n_max,0:n_max,0:nu_max,0:nu_max)
          Chi_match_lho= Chi_mnz(n_max+1:2*n_max,n_max+1:2*n_max,&
               & nu_max+1:2*nu_max,nu_max+1:2*nu_max)
          Chi_opp_wz0= Chi_mnz(2*n_max+1:3*n_max,2*n_max+1:3*n_max,&
               & 2*nu_max+1:3*nu_max,2*nu_max+1:3*nu_max)
          Chi_opp_lho= Chi_mnz(3*n_max+1:4*n_max,3*n_max+1:4*n_max,&
               & 3*nu_max+1:4*nu_max,3*nu_max+1:4*nu_max)
          Chi_first_third_match= Chi_mnz(4*n_max+1:5*n_max,4*n_max+1:5*n_max, &
               & 4*nu_max+1:5*nu_max,4*nu_max+1:5*nu_max)
          Chi_first_third_opp= Chi_mnz(5*n_max+1:6*n_max,5*n_max+1:6*n_max, &
               & 5*nu_max+1:6*nu_max,5*nu_max+1:6*nu_max)

       !Otherwise calculate it from scratch:
       ELSE

          PRINT*, "###########################################"
          PRINT*, "Generating chi lookup tables for m = ", m_set
          PRINT*, "###########################################"

          ALLOCATE( Chi_match_wz0(0:n_max,0:n_max,0:nu_max,0:nu_max), &
               & Chi_match_lho(0:n_max,0:n_max,0:nu_max,0:nu_max), &
               & Chi_opp_wz0(0:n_max,0:n_max,0:nu_max,0:nu_max), &
               & Chi_opp_lho(0:n_max,0:n_max,0:nu_max,0:nu_max), &
               & Chi_first_third_match(0:n_max,0:n_max,0:nu_max,0:nu_max), &
               & Chi_first_third_opp(0:n_max,0:n_max,0:nu_max,0:nu_max) )

          ! Matching m on chi_wz0, zero on chi_lho:
          DO n1=1,n_max+1
             DO n2=1, n_max+1
                DO nu1=1, nu_max+1
                   DO nu2=1, nu_max+1
                      CALL do_integral(n1-1,n2-1,nu1-1,nu2-1,m_set,m_set,0,0, &
                           & Chi_match_wz0(n1-1,n2-1,nu1-1,nu2-1))
                   END DO
                END DO
             END DO
          END DO

          ! Matching m on chi_lHO, zero on chi_wz0:
          DO n1=1,n_max+1
             DO n2=1, n_max+1
                DO nu1=1, nu_max+1
                   DO nu2=1, nu_max+1
                      CALL do_integral(n1-1,n2-1,nu1-1,nu2-1,0,0,m_set,m_set, &
                           & Chi_match_lho(n1-1,n2-1,nu1-1,nu2-1))
                   END DO
                END DO
             END DO
          END DO

          ! Opposite m on chi_wz0, zero on chi_lho:
          DO n1=1,n_max+1
             DO n2=1, n_max+1
                DO nu1=1, nu_max+1
                   DO nu2=1, nu_max+1
                      CALL do_integral(n1-1,n2-1,nu1-1,nu2-1,m_set,-m_set,0,0, &
                           & Chi_opp_wz0(n1-1,n2-1,nu1-1,nu2-1))
                   END DO
                END DO
             END DO
          END DO


          ! Opposite m on chi_lHO, zero on chi_wz0:
          DO n1=1,n_max+1
             DO n2=1, n_max+1
                DO nu1=1, nu_max+1
                   DO nu2=1, nu_max+1
                      CALL do_integral(n1-1,n2-1,nu1-1,nu2-1,0,0,m_set,-m_set, &
                           & Chi_opp_lho(n1-1,n2-1,nu1-1,nu2-1))
                   END DO
                END DO
             END DO
          END DO


          ! Matching m on one chi_lHO and one chi_wz0:
          DO n1=1,n_max+1
             DO n2=1, n_max+1
                DO nu1=1, nu_max+1
                   DO nu2=1, nu_max+1
                      CALL do_integral(n1-1,n2-1,nu1-1,nu2-1,m_set,0,m_set,0, &
                           & Chi_first_third_match(n1-1,n2-1,nu1-1,nu2-1))
                   END DO
                END DO
             END DO
          END DO

          ! Opposite m on one chi_lHO and one chi_wz0:
          DO n1=1,n_max+1
             DO n2=1, n_max+1
                DO nu1=1, nu_max+1
                   DO nu2=1, nu_max+1
                      CALL do_integral(n1-1,n2-1,nu1-1,nu2-1,m_set,0,-m_set,0, &
                           & Chi_first_third_opp(n1-1,n2-1,nu1-1,nu2-1))
                   END DO
                END DO
             END DO
          END DO

          ! Encoding these bloody things in one big blob for writing:
          Chi_mnz(0:n_max,0:n_max,0:nu_max,0:nu_max)=Chi_match_wz0
          Chi_mnz(n_max+1:2*n_max,n_max+1:2*n_max,nu_max+1:2*nu_max, &
               & nu_max+1:2*nu_max)=Chi_match_lho
          Chi_mnz(2*n_max+1:3*n_max,2*n_max+1:3*n_max,2*nu_max+1:3*nu_max, &
               & 2*nu_max+1:3*nu_max)=Chi_opp_wz0
          Chi_mnz(3*n_max+1:4*n_max,3*n_max+1:4*n_max,3*nu_max+1:4*nu_max, &
               & 3*nu_max+1:4*nu_max)=Chi_opp_lho
          Chi_mnz(4*n_max+1:5*n_max,4*n_max+1:5*n_max,4*nu_max+1:5*nu_max, &
               & 4*nu_max+1:5*nu_max)=Chi_first_third_match
          Chi_mnz(5*n_max+1:6*n_max,5*n_max+1:6*n_max,5*nu_max+1:6*nu_max,&
               & 5*nu_max+1:6*nu_max)=Chi_first_third_opp
          
          OPEN(ChiMNZPort, file=namestring_chi, status='new', action='write')
          WRITE(ChiMNZPort) Chi_mnz
          CLOSE(ChiMNZPort)

       END IF
    END IF
    
  END SUBROUTINE int_chi_lookup

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   do_integral
  ! VARIABLES:
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE do_integral(n1,n2,n3,n4,m1,m2,m3,m4, Int)
    INTEGER, INTENT(IN) :: n1,n2,n3,n4,m1,m2,m3,m4

    REAL(KIND=DP) :: Int, TempInt, epsabs, epsrel,abserr,r
    REAL(KIND=DP), ALLOCATABLE :: w(:), Int_array(:), r_array(:,:)
    REAL (KIND=DP) :: r1, r2
    INTEGER, ALLOCATABLE :: iw(:)
    INTEGER :: inf=1, lw=8000, liw=2000, ifail
    INTEGER :: int_limit, int_step, iN, arraysize
    !EXTERNAL :: d01bax
    !REAL (KIND=DP) :: d01baf

   
    ALLOCATE(iw(liw), w(lw))
    
    epsabs=0.000001
    epsrel=0.0001
    ifail=0

    int_limit=2000
    int_step=5

    ! Set module level variables to pass them to integrand
    module_n1=n1; module_n2=n2; module_n3=n3; module_n4=n4
    module_m1=m1; module_m2=m2; module_m3=m3; module_m4=m4

    CALL set_integral_pieces(0, int_limit, int_step, r_array, arraysize)

    ALLOCATE(Int_array(0:arraysize))

    iN=0
    
    DO WHILE (iN .LE. arraysize)

       ! Set the limits of the piece of integral:
       r1=r_array(1,iN)
       r2=r_array(2,iN)

       ! Carry out the bit of integral:
       CALL d01akf(integrand, r1, r2, epsabs, epsrel, &
            & TempInt, abserr, w, lw, iw, liw, ifail)

       !Assign the piece of integral to an array entry for later summing:
       Int_array(iN)=TempInt
       
       iN=iN+1
       
    END DO
       
    IF (ifail==1) THEN
       PRINT*, n1, n2, n3, n4
    END IF

    ! Sum all pieces for the tot integral:
    Int=SUM(Int_array)

    !Int=d01baf(d01bax, 0.0d0, 10000.0d0, 64, integrand, ifail)
    
  END SUBROUTINE do_integral


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   set_integral_pieces
  ! VARIABLES:
  !           from_r
  !           to_r
  !           step
  !           r_array
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE set_integral_pieces(from_r, to_r, step, r_array, arraysize)
    ! Interval from_r to_r has to be divisible by step.
    INTEGER, INTENT(IN) :: from_r, to_r, step
    REAL (KIND=DP), ALLOCATABLE, INTENT(OUT) :: r_array(:,:)
    INTEGER, INTENT (OUT) :: arraysize

    REAL (KIND=DP) :: A, B
    INTEGER :: iN

    arraysize=((to_r-from_r)/step)

    ALLOCATE(r_array(2,0:arraysize))

    A=from_r
    B=from_r
    iN=0

    DO WHILE (B .LT. to_r)
        A=DBLE(from_r+step*iN)
        B=DBLE(from_r+step*(iN+1))
        
        r_array(1,iN)=A
        r_array(2,iN)=B

        iN=iN+1
     END DO

  END SUBROUTINE set_integral_pieces

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   integrand
  ! VARIABLES:
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION integrand(R)
    USE global, ONLY: eta
    REAL (KIND=DP) :: integrand
    INTEGER :: n1,n2,n3,n4,m1,m2,m3,m4
    REAL (KIND=DP), INTENT(IN) :: R

    n1=module_n1; n2=module_n2; n3=module_n3; n4=module_n4
    m1=module_m1; m2=module_m2; m3=module_m3; m4=module_m4

    ! integrand=r*chi_wz0(n1,m1,r)*chi_wz0(n2,m2,r)*&
    !     & chi_lho(n3,m3,r)*chi_lho(n4,m4,r)

    integrand=(Pi/4)*R*chi(n1,m1,eta*R)*chi(n2,m2,eta*R)*chi(n3,m3,R)*chi(n4,m4,R)
    
  END FUNCTION integrand
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   generate_coeffs
  ! VARIABLES:
  !           n, m - indeces of associated Laguerre poly (IN)
  !           k - dummy index of summation in Laguerre poly (IN)
  ! SYNOPSIS:
  !   Generates the coefficient of each associated Laguerre polynomial.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION L_coeffs(n,m,k)
    USE global, ONLY: LogGamma
    REAL (KIND=DP) :: L_coeffs
    INTEGER, INTENT(IN) :: n,m,k

    !L_coeffs=((-1)**k) * fact(n+m)/( fact(n-k)*fact(m+k)*fact(k) )

    L_coeffs=((-1)**k) * EXP(LogGamma(n+m+1)-LogGamma(n-k+1)-LogGamma(m+k+1)-LogGamma(k+1))
    
  END FUNCTION L_coeffs
  
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   Laguerre
  ! VARIABLES:
  !           m
  !           n
  ! SYNOPSIS:
  !   This does the k sum in Laguerre for a given m, n 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION L(n,m,X)
    REAL (KIND=DP) :: L
    REAL (KIND=DP), ALLOCATABLE :: L_arr(:)
    INTEGER, INTENT(IN) :: m,n
    REAL (KIND=DP), INTENT(IN) :: X

    INTEGER :: k, zero

    ALLOCATE(L_arr(0:n))

    DO k=1, n+1
       L_arr(k-1)=L_coeffs(n,m,k-1)*(X**(k-1))
    END DO
    
    L=SUM(L_arr)
    
    !zero=0
    !L=coeffs(n,m,zero) ! k=0
    !DO k=1, n
    !   L=L+coeffs(n,m,k)*(X**k)
    !END DO
    
  END FUNCTION L
 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME: chi
  ! VARIABLES:
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION chi(n,m,R)
    REAL (KIND=DP) :: chi
    INTEGER, INTENT(IN) :: n, m
    REAL (KIND=DP), INTENT(IN) :: R
    REAL (KIND=DP) :: X2
   
    chi=SQRT(2*fact(n)/(Pi*fact(n+m))) * (R)**m &
         & * L(n,m,R**2) * exp(-R**2/2) * 1/SQRT(1.0d0+deltafn(m,0))
          
  END FUNCTION chi

END MODULE laguerre
