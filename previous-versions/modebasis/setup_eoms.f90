MODULE setup_eoms
  USE global, ONLY: z_R, E_0, n_max, nu_max, decode_state, encode_state, &
       & Psi, Alpha, CurrentState, steadystate, linstab,w_z0, m_A, ii, &
       & Chi_zero_m, Chi_match_wz0, Chi_match_lho, m_set, m_max, z_0, &
       & Chi_opp_wz0, Chi_opp_lho, Chi_first_third_match, sigma_pump, &
       & Chi_first_third_opp, Delta_0, z_M, kappa, hbar, l_ho, Pi, fact, &
       & statesize, omega_T, f_0, w0
  IMPLICIT NONE

  INTEGER, PRIVATE, PARAMETER :: DP=KIND(1.0D0)

  ! This module sets up the m=0 equations for time evolution to steady state.
  
CONTAINS

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   set_initial
  ! VARIABLES:
  ! SYNOPSIS:
  !   Sets Phi, Alpha and hence the current State to the initial value.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE set_initial()

    Psi=0.0d0
    Alpha=0.0d0

    Psi(0,0)=1.0d0
    !Psi(0,0)=1.0d0/SQRT(2.0d0)
    !Psi(1,0)=1.0d0/SQRT(2.0d0)
    Alpha(0,0)=100
    Alpha(1,0)=100

    CALL encode_state(Psi,Alpha,CurrentState)
    
  END SUBROUTINE set_initial

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   diffeqn
  ! VARIABLES:
  !         
  !          
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE diffeqn(t, state, ds_dt)
    REAL (KIND=DP), INTENT(IN) :: t, state(*)
    REAL (KIND=DP), INTENT (OUT) :: ds_dt(*)
    COMPLEX (KIND=DP) :: GPE(0:nu_max, 0:nu_max), Heisen(0:nu_max,0:nu_max)
    COMPLEX (KIND=DP) :: dPsi(0:nu_max,0:m_max), dAlpha(0:n_max,0:m_max)

    INTEGER :: n1,n2,nu1,nu2
    REAL (KIND=DP) :: dummy

    dummy=t 
    
    ! Decode current state vector and update global Alpha and Psi:
    CALL decode_state(state, Psi, Alpha)


    ! Building GPE matrix:
    GPE=0.0d0

    ! Diagonal elements:
    DO nu1=0, nu_max
       GPE(nu1,nu1)=-ii*omega_T*(2*nu1+1)
    END DO

    ! Off-diagonal:
    DO nu1=0, nu_max
       DO nu2=0, nu_max
          IF (nu1 .GT. nu2) THEN
             GPE(nu1,nu2)=GPE(nu1,nu2)+ii*E_0*SumGPE(nu1,nu2)
          ELSE
             GPE(nu1,nu2)=GPE(nu2,nu1)+ii*E_0*SumGPE(nu2,nu1)
          END IF
       END DO
    END DO

    !PRINT*, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    !PRINT*, GPE
    !PRINT*, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    
    dPsi(:,0)=MATMUL(GPE,Psi(:,0))
    
    ! Can now span the Psi matrix itself: (Laguerre energy is 2n+m+1)
    !DO nu1=0, nu_max
    !   dPsi(nu1,0)=-ii*omega_T*(2*nu1+1)*Psi(nu1,0) &
    !        & +(ii*E_0) *SumTerm_psi(nu1)
    !END DO

    !PRINT*, "dPsi is", dPsi

    ! Building Heisenberg eqn matrix:
    Heisen=0.0d0

    ! Diagonal elements:
    DO n1=0, n_max
       Heisen(n1,n1)=-ii*( Delta(n1,0) - ii* kappa(n1,0) )
    END DO

    ! Off-diagonal:
    DO n1=0, n_max
       DO n2=0, n_max
          Heisen(n1,n2)=Heisen(n1,n2)+ ii*E_0*SumHeisen(n1,n2)
       END DO
    END DO

    !dAlpha(:,0)=MATMUL(Heisen,Alpha(:,0))-ii*Pump()
    dAlpha=0
    
    ! And Alpha:
    !DO n1=0, n_max
    !   dAlpha(n1,0)=-ii*(Delta(n1,0)-ii*kappa(n1,0))*Alpha(n1,0) &
    !        & -ii*pumpfn(n1,0) +(ii*E_0) *SumTerm_alpha(n1)
    !END DO

    !PRINT*, "dAlpha is", dAlpha
   
    !PRINT*, "Diffeqn time", t

    CALL encode_state(dPsi, dAlpha, ds_dt)


  END SUBROUTINE diffeqn


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   SumGPE
  ! VARIABLES:
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION SumGPE(nu1,nu2)
    REAL (KIND=DP) :: SumGPE
    INTEGER, INTENT(IN) :: nu1, nu2

    COMPLEX (KIND=DP) :: SumTermArray(0:n_max,0:n_max)
    INTEGER :: n1, n2

    SumTermArray=0.0d0
    SumGPE=0.0d0
    
    DO n1=0, n_max
       DO n2=0, n_max
          SumTermArray(n1,n2)= Conjg(Alpha(n1,0)) * Alpha(n2,0) * &
               & COS((2*n1-2*n2)*(Gouy(z_0)+Pi/4)) * &
               & Chi_zero_m(n1,n2,nu1,nu2)
       END DO
    END DO

    SumGPE=REAL(SUM(SumTermArray))
    
  END FUNCTION SumGPE


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   SumHeisen
  ! VARIABLES:
  !           n1
  !           n2
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION SumHeisen(n1,n2)
    INTEGER, INTENT(IN) ::n1,n2
    COMPLEX (KIND=DP) ::SumHeisen

    COMPLEX (KIND=DP) :: SumTermArray(0:n_max,0:n_max)
    INTEGER :: nu1, nu2

       DO nu1=0, nu_max
          DO nu2=0, nu_max
             SumTermArray(nu1,nu2)= CONJG(Psi(nu1,0)) * &
                  & Psi(nu2,0) * Cos((2*n1-2*n2)*(Gouy(z_0)+Pi/4))&
                  & * Chi_zero_m(n1,n2,nu1,nu2)
          END DO
       END DO

       SumHeisen=REAL(SUM(SumTermArray))
       
  END FUNCTION SumHeisen

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   SumTerm_psi
  ! VARIABLES:
  !           nu
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION SumTerm_psi(X)
    INTEGER, INTENT(IN) :: X
    COMPLEX (KIND=DP) :: SumTerm_psi

    COMPLEX (KIND=DP) :: SumTermArray(0:n_max,0:n_max,0:nu_max)
    INTEGER :: n1,n2,nu2

    DO n1=0, n_max
       DO n2=0, n_max
          DO nu2=0, nu_max
             SumTermArray(n1,n2,nu2)= Conjg(Alpha(n1,0)) * &
                  & Alpha(n2,0) * Cos((2*n1-2*n2)*(Gouy(z_0)+Pi/4)) &
                  & * Psi(nu2,0)* Chi_zero_m(n1,n2,X,nu2)
          END DO
       END DO
    END DO
             
    SumTerm_psi=SUM(REAL(SumTermArray))

  END FUNCTION SumTerm_psi

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   SumTerm_alpha
  ! VARIABLES:
  !           n
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION SumTerm_alpha(X)
    INTEGER, INTENT(IN) :: X
    COMPLEX (KIND=DP) :: SumTerm_alpha

    COMPLEX (KIND=DP) :: SumTermArray(0:n_max,0:nu_max,0:nu_max)
    INTEGER :: n2,nu1,nu2

    DO n2=0, n_max
       DO nu1=0, nu_max
          DO nu2=0, nu_max
             SumTermArray(n2,nu1,nu2)= Conjg(Psi(nu1,0)) * &
                  & Psi(nu2,0) * Cos((2*X-2*n2)*(Gouy(z_0)+Pi/4))&
                  & * Chi_zero_m(X,n2,nu1,nu2)*Alpha(n2,0)
          END DO
       END DO
    END DO

    SumTerm_alpha=SUM(REAL(SumTermArray))

  END FUNCTION SumTerm_alpha


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   Pump
  ! VARIABLES:
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION Pump()
    REAL (KIND=DP) ::Pump(0:n_max)

    INTEGER :: k

    DO k=0, n_max
       Pump(k)=pumpfn(k,0)
    END DO
    
  END FUNCTION Pump
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   pumpfn
  ! VARIABLES:
  !           n
  !           m
  ! SYNOPSIS:
  !   The pump term as a function of quantum numbers n, m.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION pumpfn(n,m)
    INTEGER, INTENT(IN) ::n,m
    REAL (KIND=DP) :: pumpfn

    INTEGER :: k
    REAL (KIND=DP) , ALLOCATABLE :: pumpArray(:)

    ALLOCATE(pumpArray(0:n))
    
    DO k=0, n
       pumpArray(k)= ((-1)**(k)) * (fact(n) / (fact(n-k)*fact(k))) *  &
            & (2*sigma_pump/ SQRT(w(-z_M)**2+2*sigma_pump**2) )**(2*k+2)
    END DO

    pumpfn=f_0*delta(m,0)*SQRT(Pi)*w(-z_M)*0.5*SUM(pumpArray)

    DEALLOCATE(pumpArray)
    
  END FUNCTION pumpfn

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   Delta
  ! VARIABLES:
  !           n
  !           m
  ! SYNOPSIS:
  !   Returns the detuning Delta_nm.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION Delta(n,m)
    INTEGER, INTENT(IN) ::n,m
    REAL (KIND=DP) :: Delta

    Delta = Delta_0 + (2*n+m)*(Gouy(z_M)-(Pi/4))
    
  END FUNCTION Delta
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   Gouy
  ! VARIABLES:
  !           z - z-coord to calculate phase (IN)
  ! SYNOPSIS:
  !   Returns the Gouy phase at a given point in the cavity.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION Gouy(z)
    REAL(KIND=DP) :: Gouy
    REAL(KIND=DP), INTENT(IN) :: z

    Gouy=ATAN(z/z_R)
    
  END FUNCTION Gouy


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   w
  ! VARIABLES:
  !           z - the z axis coordinate (IN)
  ! SYNOPSIS:
  !   The beam waist at a point z along the cavity z axis.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION w(z)
    REAL (KIND=DP), INTENT(IN) ::z
    REAL (KIND=DP) ::w

    w=w0*SQRT(1+(z**2)/(z_M**2))
    
  END FUNCTION w
  
END MODULE setup_eoms
